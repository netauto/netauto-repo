**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

![1520435784938.JPEG.jpg](https://bitbucket.org/repo/XXzMK6x/images/342928271-1520435784938.JPEG.jpg)![0-neu-d4-2c0c0c1cb3d5b2eea50370c2a59df17f.jpg](https://bitbucket.org/repo/XXzMK6x/images/4119723839-0-neu-d4-2c0c0c1cb3d5b2eea50370c2a59df17f.jpg)